/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmo.genético;

import java.util.Random;

/**
 *
 * @author VANESSA
 */
public class AlgoritmoGenético {
    public static char [] modelo = "juan felipe montoya correa".toCharArray(); //Objetivo a alcanzar
    public static int cant_individuos = 400; //La cantidad de individuos que habra en la poblacion
    public static int pressure = 5; //Cuantos individuos se seleccionan para reproduccion. Necesariamente mayor que 2
    public static double prob_mutacion = 2; //La probabilidad de que un individuo mute
    public static int max = 50; //Valor máximo para generar el random
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("");
        System.out.println("Población modelo: ");
        for (int i = 0; i < modelo.length-1; i++) {//Mostrar el modelo
            
            System.out.print(modelo[i]+", ");
        }
        System.out.print(modelo[modelo.length-1]);
        System.out.println("");
        Individuo[] Poblacion = NuevaPoblacion(max);//Inicializar una poblacion
        System.out.println("");
        System.out.println("Primera Población");
        for (int i = 0; i < Poblacion.length; i++) {
            Poblacion[i].imprimir();
        }
        for (int i = 0; i < 150; i++) {
            Poblacion = SeleccionYReproduccion(Poblacion);
            Poblacion = Mutacion(Poblacion);
        }
        System.out.println("");
        System.out.println("Población final: ");
        for (int i = 0; i < Poblacion.length; i++) {
            Poblacion[i].imprimir(); //Se muestra la poblacion evolucionada
        }
    }
    
  
    
    public static Individuo[] NuevaPoblacion(int max) {
        //Crea una población de individuos
        Individuo[] Poblacion = new Individuo[cant_individuos];
        Individuo individuo = new Individuo();
        for (int i = 0; i < cant_individuos; i++) {
            Poblacion[i] = individuo.NuevoIndividuo(max);
        }
        return Poblacion;
    }
    
    public static void CalcularFitness(Individuo individuo){
        char[] vector = individuo.getIndividuo();
        int fitness = 0;
        for (int i = 0; i < vector.length; i++) {
            if (vector[i]==modelo[i]) {
                fitness += 1;
            }
        }
        individuo.setFitness(fitness);
    }
    
    
    public static Individuo[] OrdenarPorFitness(Individuo[] poblacion){
        for (int i = 0; i < poblacion.length; i++) {
            for (int j = i; j < poblacion.length; j++) {
                if (poblacion[i].getFitness() > poblacion[j].getFitness()) {
                    Individuo auxiliar;
                    auxiliar = poblacion[i];
                    poblacion[i] = poblacion[j];
                    poblacion[j] = auxiliar;
                }
            }
        }
        return poblacion;
    }
    
    public static Individuo[] SeleccionYReproduccion(Individuo[] poblacion){
//        Evala todos los elementos de la poblacion (population) y se queda con los mejores
//        guardandolos dentro de 'selected'.
//        Despues mezcla el material genetico de los elegidos para crear nuevos individuos y
//        llenar la poblacion (guardando tambien una copia de los individuos seleccionados sin
//        modificar).
//
//        Por ultimo muta a los individuos.
        for (Individuo poblacion1 : poblacion) { //Calcula el fitness de cada individuo
            CalcularFitness(poblacion1);
        }
        //Ordena de forma descendente la piblación respecto al fitness
        poblacion = OrdenarPorFitness(poblacion);
        //Esta linea selecciona los 'n' primeros individuos, donde n viene dado por 'pressure'
        Individuo[] seleccionados = new Individuo[poblacion.length-pressure];
        for (int i = 0; i < seleccionados.length; i++) {
            seleccionados[i] = poblacion[i+pressure];
        }
        //Se mezcla el material genético para crear nuevos individuos
        for (int i = 0; i < seleccionados.length; i++) {
            int punto = (int) (Math.random() * poblacion[i].getIndividuo().length-1) + 1;//Se elige un punto para hacer el intercambio
            int val1 = (int) (Math.random() * poblacion.length-1) + 1;
            int val2 = (int) (Math.random() * poblacion.length-1) + 1;
            while(val1 == val2) val2 = (int) (Math.random() * poblacion.length-1) + 1;
            Individuo padre1 = poblacion[val1];
            Individuo padre2 = poblacion[val2];//Se eligen dos padres
            for (int j = punto; j < poblacion[i].getIndividuo().length; j++) {
                poblacion[i].getIndividuo()[j] = padre2.getIndividuo()[j];
            }
            for (int j = punto; j >= 0; j--) {
                poblacion[i].getIndividuo()[j] = padre1.getIndividuo()[j];
            }
        }
        return poblacion; //El array 'population' tiene ahora una nueva poblacion de individuos, que se devuelven
    }
    public static Individuo[] Mutacion(Individuo[] poblacion){ //Se mutan los individuos al azar. Sin la mutacion de nuevos genes nunca podria alcanzarse la solucion.
        for (int i = 0; i < poblacion.length-pressure; i++) {
            double random = Math.random() * 1;
            if (random <= prob_mutacion) {
                Random r = new Random();
                int punto = (int) (Math.random() * poblacion[i].getIndividuo().length-1) + 1;//Se elige un punto al azar
                char nuevo_valor = (char) (r.nextInt(26) + 'a');//y un nuevo valor para este punto
                while (nuevo_valor == poblacion[i].getIndividuo()[punto])
                    nuevo_valor = (char)(r.nextInt(26) + 'a');//Es importante mirar que el nuevo valor no sea igual al viejo
                poblacion[i].getIndividuo()[punto] = (char) nuevo_valor;
            }
        }
        return poblacion;
    }
    
}
