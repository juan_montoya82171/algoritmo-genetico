/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmo.genético;

import static algoritmo.genético.AlgoritmoGenético.modelo;
import java.util.Random;

/**
 *
 * @author VANESSA
 */
public class Individuo {
    
    private int longitud = 26; //La longitud del material genetico de cada individuo
    private int fitness; //Fitness del individuo
    private char [] individuo; //Vector que contiene los datos del individuo
    
    public Individuo NuevoIndividuo(int max) {
        //Crea un individual
        Individuo Nuevo_Individuo = new Individuo();
        char [] vector = new char[longitud];
        String alphabet = "abcdefghijflmnopqrstuvwxy ";
        Random r = new Random();
        for (int i = 0; i < longitud; i++) {
            vector[i] = (char) (alphabet.charAt(r.nextInt(alphabet.length())));
        }
        Nuevo_Individuo.setIndividuo(vector);
        return Nuevo_Individuo;
    }

    public void imprimir(){
        System.out.println("");
        for (int i = 0; i < this.individuo.length-1; i++) {//Mostrar el modelo
            
            System.out.print(this.individuo[i]+", ");
        }
        System.out.print(this.individuo[this.individuo.length-1]);
        System.out.println("");
    }
    
    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public int getFitness() {
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    public char[] getIndividuo() {
        return individuo;
    }

    public void setIndividuo(char[] individuo) {
        this.individuo = individuo;
    }
    
    
    
}
